<?php

class Dispatcher
{
	private $request;

	public function dispatch()
    {
        $this->request = new Request();
        // Handle URL
        Router::parse($this->request->url, $this->request);

        $controller = $this->loadController();
        call_user_func_array([$controller, $this->request->action], $this->request->params);
    }

    public function loadController()
    {
        $controller = $this->request->controller . "Controller";
        $action = $this->request->action;
        $model = $this->request->controller;

        // echo "<br>controller :  " . $controller . ", model: " .$model . ", action : " . $action . "<br>";

        require_once(ROOT . DS. 'Controllers'. DS . $controller . '.php');
        require_once(ROOT . DS. 'Models'. DS . $model . '.php');

        $controller = new $controller($controller, $model, $action);

        return $controller;
    }
}
?>
