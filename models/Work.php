<?php

use library\Model;

class Work extends Model
{
    function __construct()
    {
        
    }

    public function showAllWorks()
    {
        $sql = "SELECT * FROM work";

        $this->_result = Work::getDB()->query($sql);

        return $this->_result;
    }
    
    public function createWork($workName, $startingDate, $endingDate, $status)
    {
        $sql = "INSERT INTO work (work_name, starting_date, ending_date, status) VALUES ('$workName', '$startingDate', '$endingDate', $status)";

        //$sql = "INSERT INTO work (work_name, status) VALUES ('$workName', $status)";
         echo $sql;
        $this->_result = Work::getDB()->exec($sql);

        return $this->_result;

    }

    public function findWork($idWork)
    {
        $sql = "SELECT * FROM work WHERE id = $idWork";
        $this->_result = Work::getDB()->query($sql);
        return $this->_result->fetch();
    }

    public function updateWork($idWork, $workName, $startingDate, $endingDate, $status)
    {
        $sql = "UPDATE work SET work_name = '$workName', starting_date = '$startingDate', ending_date = '$endingDate', status = $status WHERE id = $idWork";
        
        $this->_result = Work::getDB()->exec($sql);
        return $this->_result;
    }

    public function deleteWork($idWork)
    {
        $sql = "DELETE FROM work WHERE id = $idWork";
        echo $sql;
        $this->_result = Work::getDB()->exec($sql);
        return $this->_result;
    }

}
?>