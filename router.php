<?php

class Router
{
    static public function parse($url, $request)
    {
        $url = trim($url);
        //echo "url: " . $url;
        if ($url == "/todolist1/")
        {
            
            $request->controller = "work";
            $request->action = "list";
            $request->params = [];
        }
        else if ($url == "/todolist1/calendar")
        {
            $request->controller = "calendar";
            $request->action = "index";
            $request->params = [];
        }
        else
        {

            $explode_url = explode('/', $url);
            $explode_url = array_slice($explode_url, 2);
            
            if (!isset($explode_url[1])|| $explode_url[1] == ""){
                $request->action = "list";
            }
            else{
                $request->action = $explode_url[1];
            }
            $request->params = array_slice($explode_url, 2);

            $request->controller = $explode_url[0];
        }
    }

}
?>
