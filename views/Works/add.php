<a href="./" class="btn btn-primary">Back</a>

<form method='post' action='/todolist1/work/submit_add'>
    <div class="form-group">
        <label">Work name</label>
        <input type="text" class="form-control" id="workName" name="workName" placeholder="Work name" required>
        <label">Starting date name</label>
        <input type="date" class="form-control" id="startingDate" name="startingDate" placeholder="Starting date">
        <label">Ending date</label>
        <input type="date" class="form-control" id="endingDate" name="endingDate" placeholder="Ending date">
        <label">Status</label>
        <select class="form-control" id="status" name="status">
            <option value="0">Planing</option>
            <option value="1">Doing</option>
            <option value="2">Complete</option>
        </select>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>