
<a href="/todolist1/work/add" class="btn btn-primary">Add</a>

<a href="/todolist1/calendar" class="btn btn-primary">Calendar view</a>

<br/>
<h1>List Work</h1>
<div class="row">
<table class="table">
    <thead>
        <tr>
            <th>ID</th>
            <th>Work name</th>
            <th>Ending date</th>
            <th>Staring date</th>
            <th>Status</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
    </thead>

    <tbody>
    <?php
        $checkNull = true;
        if (is_array($listWork) || is_object($listWork))
            {
                foreach ($listWork as $work) {
                    $checkNull = false;
        ?>
                    <tr>
                        <td><?= $work['id'] ?></td>
                        <td><?= $work['work_name'] ?></td>
                        <td><?= $work['starting_date'] ?></td>
                        <td><?= $work['ending_date'] ?></td>
                        <td>
                        <?php 
                            switch ($work['status']) {
                                case 0:
                                    echo "Planing";
                                    break;
                                case 1:
                                    echo "Doing";
                                    break;
                                case 2:
                                    echo "Complete";
                                    break;
                                default:
                                    echo "None";
                            }
                        ?>
                        </td>
                        <td><a class='btn btn-info btn-xs' href="/todolist1/work/edit/<?= $work["id"]; ?>"><span class='glyphicon glyphicon-edit'></span> Edit</a></td>

                        <td><a class='btn btn-info btn-danger' href="/todolist1/work/delete/<?= $work["id"]; ?>"><span class='glyphicon glyphicon-edit'></span>Delete</a></td>
                    </tr>
        <?php
                }
            }
        if ($checkNull) {
            echo "<p>None list work</p>";
        }
        ?>
    </tbody>

  </div>