<h3>Edit work</h3>
<a href="/todolist1/work/" class="btn btn-primary">Back</a>

<form method='post' action='/todolist1/work/submit_edit'>
    <div class="form-group">
        <label">Work name</label>
        <input type="text" class="form-control" id="workName" name="workName" value ="<?php if (isset($work["work_name"])) echo $work["work_name"]; ?>" placeholder="Work name" required>
        <label">Starting date name</label>
        <input type="date" class="form-control" id="startingDate" name="startingDate" value="<?php echo $work['starting_date']; ?>" placeholder="Starting date">
        <label">Ending date</label>
        <input type="date" class="form-control" id="endingDate" name="endingDate" value="<?php echo $work['ending_date']; ?>" placeholder="Ending date">
        <label">Status</label>
        <select class="form-control" id="status" name="status">
            <option value="0" <?php if($work['status'] == 0)  echo "selected"; ?>>Planing</option>
            <option value="1" <?php if($work['status'] == 1)  echo "selected"; ?>>Doing</option>
            <option value="2" <?php if($work['status'] == 2)  echo "selected"; ?> >Complete</option>
        </select>
    </div>
    <input type="hidden" id="idWork" name="idWork" value="<?php echo $work['id']; ?>">
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

