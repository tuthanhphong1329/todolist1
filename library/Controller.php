<?php
namespace library;

class Controller
{

	protected $vars = [];

	protected $_model;
	protected $_controller;
	protected $_action;

	function __construct($controller, $model, $action)
	{
		global $inflect;

		$this->_controller = $controller;
		$this->_action = $action;
		$this->_model = new $model;
	}

	function set($d)
	{
		$this->vars = array_merge($this->vars, $d);
	}

	function render($controller, $action)
	{
		extract($this->vars);
		include (ROOT . DS .  'views' . DS . 'header.php');

		// contain
		include (ROOT . DS .  'views' . DS . $controller . DS . $action .'.php');
		//

		include (ROOT . DS .  'views' . DS . 'footer.php');
	}

	function __destruct()
	{
		
	}
}

?>

