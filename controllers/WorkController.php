<?php

use library\Controller;

class WorkController extends Controller
{
    function list()
    {
        $work = new Work();
        $data['listWork'] = $work->showAllWorks();

        $this->set($data);	
        $this->render("works", "list");
    }

    function add()
    {
            $this->render("works", "add");
    }

    function submit_add()
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") 
        {
            $work = new Work();

            // collect value of input field
            $workName = $_POST['workName'];
            $startingDate = $_POST['startingDate'];
            $endingDate = $_POST['endingDate'];
            $status = $_POST['status'];

            if ($work->createWork($workName, $startingDate, $endingDate, $status))
            {
                $startingDateFormat = date("Y-m-d", strtotime($startingDate));
                $endingDateFormat = date("Y-m-d", strtotime($startingDate));

                echo $workName . "  " . $startingDateFormat . " " . $endingDateFormat . " - " . $status;
            
                header("Location: http://localhost:8080/todolist1/work");
                exit();
            }
        }
    }
    
    function edit($id)
    {
        $work = new Work();
        $data['work'] = $work->findWork($id);

        $this->set($data);
        $this->render("works", "edit");
    }

    function submit_edit()
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") 
        {
            $work = new Work();

            // collect value of input field
            $id = $_POST['idWork'];
            $workName = $_POST['workName'];
            $startingDate = $_POST['startingDate'];
            $endingDate = $_POST['endingDate'];
            $status = $_POST['status'];

            if ($work->updateWork($id, $workName, $startingDate, $endingDate, $status) >= 0)
            {
                header("Location: http://localhost:8080/todolist1/work");
                exit();
            }
        }
    }

    function delete($id)
    {
        $work = new Work();
        if($work->deleteWork($id))
        {
            header("Location: http://localhost:8080/todolist1/work");
            exit();
        }
        else
        {
            $this->render("works", "list");
        }
    }

}
?>
