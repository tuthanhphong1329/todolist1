<?php

//echo "-----------root.php";
define('ROOT', dirname(dirname(__FILE__)));
define('DS', DIRECTORY_SEPARATOR);

require_once(ROOT . DS . "library/Model.php");
require_once(ROOT . DS . "library/Controller.php");

require_once(ROOT . DS . 'config.php');
require_once(ROOT . DS . 'dispatcher.php');
require_once(ROOT . DS . 'request.php');
require_once(ROOT . DS . 'router.php');

$dispatch = new Dispatcher();
$dispatch->dispatch();

?>